const item = {
    id:'',
    designation:'',
    done: false
}
/**
 * --------------------------
 */

let task = {};

Object.defineProperties(task, {
  id: {
    value: 1,
    writable: true
  },
  designation: {
    value: '',
    writable: true
  },
  done: {
    value: false,
    writable: true
  },
});

console.log('task', task);

let tasksList = [];


function addTask() {
    document.querySelector('ul').innerText = '';
    document.querySelector('#deleteOne').innerText = '';

    const val = document.querySelector('input').value;
    task = {
        id: task.id, //'number en AI, je cherche comment'
        designation: val,
        done: false
    }

    tasksList.push(task);
    console.log('taskList', tasksList);
    
    tasksList.forEach(task => {
        document.querySelector('ul').innerHTML += '<li>' + task.designation + '</li>';
        document.querySelector('#deleteOne').innerHTML += '<button id="'+ task.id +'" class="button is-danger is-outlined is-small">X</button>';
    });
}

function clearList() {
    tasksList = [];
    // console.log('toto');
    // console.log('tasksListInFct',tasksList);
    document.querySelector('ul').innerText = '';
    document.querySelector('#deleteOne').innerText = '';
}